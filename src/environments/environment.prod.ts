export const environment = {
  production: true,
  accessApi: 'http://localhost:8085/api/',
  paymentApi: 'https://platform-payment.azurewebsites.net/api/',
  paymentMovid: 'http://localhost:8080/api/',
  parkingMovid: 'https://parking-movid-admin.azurewebsites.net/api/',
  locationApi: 'http://localhost:8081/api/',
  openPayMerchantId: 'mkfwcatzfwdarfpihtqq',
  openPayApiKey: 'pk_58a5e00ff8ff4ba6ad665f3c0096a2e4',
  openPayApiURL: 'https://sandbox-api.openpay.co/v1/'
};