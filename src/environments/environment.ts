// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //accessApi: 'http://localhost:8085/api/',
  accessApi: 'https://platform-access.azurewebsites.net/api/',
  paymentApi: 'https://platform-payment.azurewebsites.net/api/',
  //paymentMovid: 'http://localhost:8080/api/',
  paymentMovid: 'https://parking-movid-payment.azurewebsites.net/api/',
  //parkingMovid: 'https://parking-movid-admin.azurewebsites.net/api/',
  parkingMovid: 'http://localhost:8180/api/',
  locationApi: 'http://localhost:8081/api/',
  openPayMerchantId: 'mkfwcatzfwdarfpihtqq',
  openPayApiKey: 'pk_58a5e00ff8ff4ba6ad665f3c0096a2e4',
  openPayApiURL: 'https://sandbox-api.openpay.co/v1/',
  locationTempToken: 'Bearer eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJsdWh5IiwiYXVkIjoiUk9MRV9BRE1JTiIsImlhdCI6MTU5NzQxNTEyNiwiZXhwIjoxNTk3NTAxNTI2fQ.PwHujcN6q14Olw4dOSaQGYWt1z-4p64EitGRTZGsLiTgO2CA-pbrsCP-U7uepKBO'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
