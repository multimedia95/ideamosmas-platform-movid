import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatPipe } from './date.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    DateFormatPipe
  ],
  declarations: [
    DateFormatPipe
  ]
})
export class PipeModule { }