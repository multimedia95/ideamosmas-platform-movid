import { Pipe, PipeTransform } from '@angular/core';
import { format } from 'date-fns';
import { es } from 'date-fns/locale'
import { UtilsService } from '../services/utils.service';

@Pipe({ name: 'dateFormat' })
export class DateFormatPipe implements PipeTransform {
    constructor(
        private utilsService: UtilsService
    ) {

    }
    transform(date: string, fmt: string): string {
        let formatDate = format(this.utilsService.convertDateForIos(date), fmt, { locale: es })
        return formatDate;
    }
}