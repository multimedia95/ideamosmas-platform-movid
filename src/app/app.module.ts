import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ImgModalPageModule } from './pages/img-modal/img-modal.module';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { HttpClientModule } from '@angular/common/http'
import { AuthGuard } from './guards/auth-guard.service';
import { ToastComponent } from './components/toast/toast.component';

import { Device } from '@ionic-native/device/ngx';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx'
import { FCM } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/ionic/ngx/FCM';
import { NgCalendarModule } from 'ionic2-calendar';
import { CalendarModalPageModule } from './pages/calendar-modal/calendar-modal.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    ImgModalPageModule,
    HttpClientModule,
    NgCalendarModule,
    CalendarModalPageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    ToastComponent,
    Facebook,
    FCM,
    Device,
    AuthGuard,
    LaunchNavigator,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
