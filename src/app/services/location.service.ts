import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    url: string;

    constructor(
        private http: HttpClient,
    ) {
        this.url = environment.locationApi;
    }

    findAllImagesObjectById(authorizationToken, id): Observable<any> {
        const url = `${this.url}objectImage/findAllByIdObjectUrl/${id}`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        });
    }

    findDocumentById(authorizationToken, id): Observable<any> {
        const url = `${this.url}document/fileToJson`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            },
            params: {
                objectName: id
            }
        });
    }

}