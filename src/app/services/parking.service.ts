import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ParkingService {

    url: string;

    constructor(
        private http: HttpClient,
    ) {
        this.url = environment.parkingMovid;
    }

    getParkingDetails(authorizationToken: string, parkingID): Observable<any> {
        const url = `${this.url}parking/${parkingID}`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    getParkingTags(authorizationToken: string, parkingId): Observable<any> {
        const url = `${this.url}tagsParking/`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            },
            params: {
                parkingId: parkingId
            }
        })
    }

    getTags(authorizationToken: string): Observable<any> {
        const url = `${this.url}tag/findAll/`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }
    // authorization should be remove later
    getParkingMarkers(arrivalDate: string, arrivalHour: string, latitude: string, longitude: string, vehicleTypeId: string): Observable<any> {
        const url = `${this.url}parking/filter`;
        return this.http.get(url, {
            params: {
                arrivalDate,
                arrivalHour,
                latitude,
                longitude,
                vehicleTypeId
            }
        })
    }

    getVehicleTypes(authorizationToken: string): Observable<any> {
        const url = `${this.url}vehicleType/findAll`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }
}