import { Injectable, EventEmitter } from '@angular/core';
import { FCM } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/ionic/ngx/FCM';

@Injectable({
	providedIn: 'root'
})
export class MessagingService {
	operationChange: EventEmitter<any> = new EventEmitter();

	constructor(private fcm: FCM) { }

	getToken() {
		return this.fcm.getToken();
	}

	refreshToken() {
		return this.fcm.onTokenRefresh();
	}

	receiveMessage() {
		return this.fcm.onNotification();
	}
}
