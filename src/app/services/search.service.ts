import { Injectable, EventEmitter } from '@angular/core';
import { GooglePlaceModel } from '../models/google-place.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  citySelected: EventEmitter<any> = new EventEmitter();
  locationSelected: EventEmitter<any> = new EventEmitter();

  constructor() { }

}