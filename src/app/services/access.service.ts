import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { VehicleModel } from '../models/vehicle.model';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  url: string;
  constructor(
    public http: HttpClient
  ) {
    this.url = environment.accessApi;
  }

  getUserInformation(authorizationToken): Observable<any> {
    const url = `${this.url}user`;
    return this.http.get(url, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

  saveVehicle(authorizationToken, vehicle: VehicleModel): Observable<any> {
    const url = `${this.url}user/vehicle`;
    return this.http.put(url, vehicle, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

  setFavoriteVehicle(authorizationToken, vehicle: { vehicleId: string }): Observable<any> {
    const url = `${this.url}user/vehicle-id`;
    return this.http.put(url, vehicle, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

  deleteVehicle(authorizationToken, vehicle: VehicleModel): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        'Authorization': authorizationToken,
      }),
      body: vehicle,
    };
    const url = `${this.url}user/vehicle`;
    return this.http.delete(url, options)
  }

  getInformationCode(authorizationToken): Observable<any> {
    const url = `${this.url}user/code`;
    return this.http.get(url, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

  getLastOperation(authorizationToken: string): Observable<any> {
    const url = `${this.url}parking/operation/last`;
    return this.http.get(url, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

  getLastPayment(authorizationToken: string, operationId: string) {

    const url = `${this.url}parking/payment/${operationId}`;
    return this.http.get(url, {
      headers: {
        Authorization: authorizationToken
      }
    })
  }

}
