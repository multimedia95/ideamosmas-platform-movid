import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { OpenpayPayment } from '../models/openpay-payment.model';

@Injectable({
    providedIn: 'root'
})
export class OpenPayService {

    url: string;
    urlPayment: string;

    constructor(
        private http: HttpClient,
    ) {
        this.url = environment.accessApi;
        this.urlPayment = environment.paymentApi;
    }

    saveCreditCard(authorizationToken: string, body): Observable<any> {
        const url = `${this.url}user/openpay`;
        return this.http.post(url, body, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    creditCardInfo(authorizationToken: string, cardId): Observable<any> {
        const url = `${this.url}user/cards/openpay/${cardId}`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    creditCardPayment(authorizationToken: string, payment: OpenpayPayment): Observable<any> {
        const url = `${this.urlPayment}payment/openpay`;
        return this.http.post(url, payment, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    savePayment(authorizationToken: string, payment): Observable<any> {
        const url = `${this.urlPayment}payment/parking/save`;
        return this.http.post(url, payment, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }
}