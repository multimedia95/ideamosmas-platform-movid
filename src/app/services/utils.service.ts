import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  days = {
    1: 'Domingo',
    2: 'Lunes',
    3: 'Martes',
    4: 'Miércoles',
    5: 'Jueves',
    6: 'Viernes',
    7: 'Sábado'
  }
  constructor() { }

  scheduleDays(from, to) {
    if (from === to) {
      return this.days[from];
    } else if (from === 1 && to === 7) {
      return 'Lunes a Domingo'
    } else {
      return `${this.days[from]} a ${this.days[to]}`;
    }
  }

  convertDateForIos(date) {
    var arr = date.split(/[- :]/);
    date = new Date(arr[0], arr[1] - 1, arr[2], arr[3], arr[4], arr[5]);
    return date;
  }
}
