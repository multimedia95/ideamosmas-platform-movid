import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SocialMediaLogin } from '../models/social-media-login.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    authState = new BehaviorSubject(false);
    accessApi = '';
    constructor(
        private storage: Storage,
        private platform: Platform,
        private http: HttpClient
    ) {
        this.platform.ready().then(() => {
            this.checkToken();
        })
        this.accessApi = environment.accessApi;
    }

    logout() {
        this.authState.next(false);
        this.storage.remove('Authorization');
        this.storage.remove('Expiration');
    }

    isAuthenticated(): boolean {
        return this.authState.value;
    }

    checkToken() {
        this.storage.get('Authorization').then(resp => {
            if (resp) {
                this.verifyExpirationTime();
            }
        });
    }

    saveAuthorizationToken(data) {
        this.storage.set('Authorization', data.authorizationToken).then(() => {
            this.storage.set('Expiration', data.expiration);
            this.authState.next(true);
        });
    }

    verifyExpirationTime() {
        this.storage.get('Expiration').then(resp => {
            if (resp) {
                const date = new Date(resp);

                if (date > new Date()) {
                    this.authState.next(true);
                } else {
                    this.authState.next(false);
                }
            }
        });
    }

    passwordLogin(user): Observable<any> {
        const url = `${this.accessApi}login/password`;
        return this.http.post(url, user);
    }

    register(user): Observable<any> {
        const url = `${this.accessApi}register/otp`;
        return this.http.put(url, user);
    }

    otpRequest(user): Observable<any> {
        const url = `${this.accessApi}register/otp/email`;
        return this.http.post(url, user);
    }

    savePassword(authorizationToken: string, body): Observable<any> {
        const url = `${this.accessApi}register/password`
        return this.http.post(url, body, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    validateSocialLogin(socialMediaData: SocialMediaLogin): Observable<any> {
        return this.http.post(this.accessApi + 'login/social', socialMediaData);
    }

}