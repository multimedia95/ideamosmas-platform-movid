import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { SubscriptionModel } from '../models/subscription.model';

@Injectable({
    providedIn: 'root'
})
export class PaymentService {

    url: string;
    paymentApi: string;

    constructor(
        private http: HttpClient,
    ) {
        this.url = environment.paymentMovid;
        this.paymentApi = environment.paymentApi;
    }

    getSubscriptionPricesByCompany(authorizationToken: string, companyID): Observable<any> {
        const url = `${this.url}subscriptionPrice/company/${companyID}`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    getScheduleGroup(authorizationToken: string, parkingId: string): Observable<any> {
        const url = `${this.url}ScheduleGroup/`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            },
            params: {
                parkingId
            }
        })
    }

    subscription(authorizationToken: string, subscription: SubscriptionModel): Observable<any> {
        const url = `${this.url}subscription`;
        return this.http.post(url, subscription, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    calculateSubscription(authorizationToken: string, subscription): Observable<any> {
        const url = `${this.url}subscription/calculate`;
        return this.http.post(url, subscription, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    subscriptionParking(authorizationToken: string, subscriptionParking): Observable<any> {
        const url = `${this.url}subscriptionParking/`;
        return this.http.post(url, subscriptionParking, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    availability(authorizationToken: string, startDate, endDate, parkingId, vehicleTypeId): Observable<any> {
        const url = `${this.url}subscription/capacity`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            },
            params: {
                startDate,
                endDate,
                parkingId,
                vehicleTypeId
            }
        })
    }

    getPaymentMethods(authorizationToken: string): Observable<any> {
        const url = `${this.url}paymentMethod/findAll`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

    getSubscriptions(authorizationToken: string, userId): Observable<any> {
        const url = `${this.url}subscription/filter`;
        return this.http.get(url, {
            headers: {
                Authorization: authorizationToken
            },
            params: {
                additionalCode: userId
            }
        })
    }

    getPaymentInformation(authorizationToken: string, body) {
        const url = `${this.paymentApi}payment/parking/calculate`;
        return this.http.post(url, body, {
            headers: {
                Authorization: authorizationToken
            }
        })
    }

}