import { Component } from "@angular/core";
import { ToastController } from "@ionic/angular";

@Component({
	selector: "app-toast",
	templateUrl: "./toast.component.html"
})
export class ToastComponent {
	constructor(public toastController: ToastController) {}

	async presentToast(message: string, color: string) {
		const toast = await this.toastController.create({
			message,
			duration: 2000,
			color
		});
		toast.present();
	}
}
