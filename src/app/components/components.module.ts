import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { CreditCardBasicInfoComponent } from './credit-card-basic-info/credit-card-basic-info.component';
import { ParkingCardInfoComponent } from './parking-card-info/parking-card-info.component';
import { VehicleDetailsCardComponent } from './vehicle-details-card/vehicle-details-card.component';
import { ToastComponent } from './toast/toast.component';


@NgModule({
    declarations: [
        CreditCardBasicInfoComponent,
        ParkingCardInfoComponent,
        VehicleDetailsCardComponent,
        ToastComponent
    ],
    imports: [CommonModule, FormsModule, IonicModule],
    exports: [
        CreditCardBasicInfoComponent,
        ParkingCardInfoComponent,
        VehicleDetailsCardComponent,
        ToastComponent
    ]
})
export class ComponentsModule { }