import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { VehicleModel } from 'src/app/models/vehicle.model';

@Component({
  selector: 'app-vehicle-details-card',
  templateUrl: './vehicle-details-card.component.html',
  styleUrls: ['./vehicle-details-card.component.scss'],
})
export class VehicleDetailsCardComponent implements OnInit {

  @Input() vehicleDetails: VehicleModel;
  @Input() defaultVehicle: string;
  @Output() onDelete = new EventEmitter<VehicleModel>();
  @Output() onFavorite = new EventEmitter<VehicleModel>();

  constructor() { }

  ngOnInit() { }

  getVehicleImg(type) {
    switch (type) {
      case 'CAR':
        return '../../../assets/img/car.png';
      case 'MOTORCYCLE':
        return '../../../assets/img/motorcycle.png';
      case 'BICYCLE':
        return '../../../assets/img/bicycle.png';
      case 'PUBLIC':
        return '../../../assets/img/public.png';
      case 'TRUCK':
        return '../../../assets/img/truck.png';
      default:
        return '';
    }
  }

  deleteVehicle() {
    this.onDelete.emit(this.vehicleDetails);
  }

  setToFavorite() {
    this.onFavorite.emit(this.vehicleDetails);
  }

}
