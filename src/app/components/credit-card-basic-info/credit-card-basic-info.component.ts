import { Component, OnInit, Input } from '@angular/core';
import { CardInfoModel } from 'src/app/models/card-info.model';

@Component({
  selector: 'app-credit-card-basic-info',
  templateUrl: './credit-card-basic-info.component.html',
  styleUrls: ['./credit-card-basic-info.component.scss'],
})
export class CreditCardBasicInfoComponent implements OnInit {

  @Input() cardInformation: CardInfoModel;

  constructor() { }

  ngOnInit() {

  }

  getCardImg(brand) {
    switch (brand) {
      case 'visa':
        return '../../../assets/img/Former_Visa_logo.svg';
      case 'mastercard':
        return '../../../assets/img/Mastercard-logo.svg';
      case 'american_express':
        return '../../../assets/img/American_Express_logo.svg';
      default:
        return '';
    }
  }

}
