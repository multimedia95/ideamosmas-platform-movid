import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Parking } from 'src/app/models/parking.model';

@Component({
  selector: 'app-parking-card-info',
  templateUrl: './parking-card-info.component.html',
  styleUrls: ['./parking-card-info.component.scss'],
})
export class ParkingCardInfoComponent implements OnInit {

  @Output() onSelected = new EventEmitter<any>();
  @Input() parkingDetails;

  constructor() { }

  ngOnInit() { }

  onCardSelected() {
    this.onSelected.emit(null);
  }

}
