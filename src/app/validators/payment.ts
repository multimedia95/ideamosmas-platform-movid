import { FormControl } from '@angular/forms';
declare var OpenPay: any;

export class PaymentValidator {

    static isValidCardNumber(control: FormControl): any {

        if (!OpenPay.card.validateCardNumber(control.value)) {
            return {
                "Número de tarjeta inválido": true
            };
        }

        return null;
    }

    static isValidCVC(control: FormControl): any {

        if (!OpenPay.card.validateCVC(control.value)) {
            return {
                "Código de seguridad inválido": true
            };
        }

        return null;
    }

    static isValidMonth(control: FormControl): any {

        if (Number(control.value) < 1 || Number(control.value) > 12) {
            return {
                "Mes invalido.": true
            };
        }

        return null;
    }

    static isValidYear(control: FormControl): any {

        if (Number(control.value) > 99 || Number(control.value) < 1) {
            return {
                "Año invalido.": true
            };
        }

        return null;
    }

}