import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'account/payment-methods',
    loadChildren: () => import('./pages/payment-methods/payment-methods.module').then(m => m.PaymentMethodsPageModule)
  },
  {
    path: 'account/payment-methods/new-payment-method',
    loadChildren: () => import('./pages/new-payment-method/new-payment-method.module').then(m => m.NewPaymentMethodPageModule)
  },
  {
    path: 'account/my-details',
    loadChildren: () => import('./pages/my-details/my-details.module').then(m => m.MyDetailsPageModule)
  },
  {
    path: 'account/my-vehicles',
    loadChildren: () => import('./pages/my-vehicles/my-vehicles.module').then(m => m.MyVehiclesPageModule)
  },
  {
    path: 'account/my-vehicles/new-vehicle',
    loadChildren: () => import('./pages/new-vehicle/new-vehicle.module').then(m => m.NewVehiclePageModule)
  },
  {
    path: 'choose-vehicle',
    loadChildren: () => import('./pages/choose-vehicle/choose-vehicle.module').then(m => m.ChooseVehiclePageModule)
  },
  {
    path: 'search-location',
    loadChildren: () => import('./pages/search-location/search-location.module').then(m => m.SearchLocationPageModule)
  },
  {
    path: 'parking-details/:id',
    loadChildren: () => import('./pages/parking-details/parking-details.module').then(m => m.ParkingDetailsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'parking-details/:id/summary',
    loadChildren: () => import('./pages/summary/summary.module').then(m => m.SummaryPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'help',
    loadChildren: () => import('./pages/help/help.module').then(m => m.HelpPageModule)
  },
  {
    path: 'reservation-details',
    loadChildren: () => import('./pages/reservation-details/reservation-details.module').then(m => m.ReservationDetailsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'qr-code',
    loadChildren: () => import('./pages/qr-code/qr-code.module').then( m => m.QrCodePageModule)
  },
  {
    path: 'card-payment',
    loadChildren: () => import('./pages/card-payment/card-payment.module').then( m => m.CardPaymentPageModule)
  },
  {
    path: 'otp-confirmation',
    loadChildren: () => import('./pages/otp-confirmation/otp-confirmation.module').then( m => m.OtpConfirmationPageModule)
  },
  {
    path: 'calendar-modal',
    loadChildren: () => import('./pages/calendar-modal/calendar-modal.module').then( m => m.CalendarModalPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
