import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationDetailsPageRoutingModule } from './reservation-details-routing.module';

import { ReservationDetailsPage } from './reservation-details.page';
import { QRCodeModule } from 'angularx-qrcode';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { QrcodeModalPageModule } from '../qrcode-modal/qrcode-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservationDetailsPageRoutingModule,
    QRCodeModule,
    PipeModule,
    QrcodeModalPageModule
  ],
  declarations: [ReservationDetailsPage]
})
export class ReservationDetailsPageModule { }
