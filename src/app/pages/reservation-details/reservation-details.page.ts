import { Component, OnInit } from '@angular/core';
import { Parking } from 'src/app/models/parking.model';
import { ModalController } from '@ionic/angular';
import { QrcodeModalPage } from '../qrcode-modal/qrcode-modal.page';
import { ActivatedRoute, Router } from '@angular/router';
import { SubscriptionModel } from 'src/app/models/subscription.model';
import { ParkingService } from 'src/app/services/parking.service';
import { Storage } from '@ionic/storage';
import { UtilsService } from 'src/app/services/utils.service';
import { ScheduleGroupModel } from 'src/app/models/schedule-group.model';
import { PaymentService } from 'src/app/services/payment.service';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-reservation-details',
  templateUrl: './reservation-details.page.html',
  styleUrls: ['./reservation-details.page.scss'],
})
export class ReservationDetailsPage implements OnInit {

  subscription: SubscriptionModel;
  parking: Parking;
  authorizationToken: string;
  scheduleGroups: ScheduleGroupModel[];
  myLatLng: {
    lat: number;
    lng: number;
  };

  constructor(
    private geolocation: Geolocation,
    private launchNavigator: LaunchNavigator,
    private modalController: ModalController,
    private parkingService: ParkingService,
    private utilsService: UtilsService,
    private paymentService: PaymentService,
    private route: ActivatedRoute,
    private storage: Storage,
    private router: Router
  ) { }

  async ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.subscription = this.router.getCurrentNavigation().extras.state.subscription;
        console.log(this.router.getCurrentNavigation().extras.state);
        this.storage.get('Authorization').then(token => {
          this.authorizationToken = token;
          this.parkingService.getParkingDetails(token, this.subscription.parkingId).subscribe(data => {
            this.parking = data;
          }, error => {
            console.log(error);
          })
          this.paymentService.getScheduleGroup(this.authorizationToken, this.subscription.parkingId).subscribe(data => {
            this.scheduleGroups = data;
            console.log(data)
          }, error => {
            console.log(error);
          })
        })
      }
    })
    this.myLatLng = await this.getLocation();
  }

  openPreview(data) {
    this.modalController.create({
      component: QrcodeModalPage,
      componentProps: {
        data: data
      }
    }).then(modal => modal.present());
  }

  scheduleDays(from, to) {
    return this.utilsService.scheduleDays(from, to);
  }

  navigateTo() {
    let options: LaunchNavigatorOptions = {
      start: [this.myLatLng.lat, this.myLatLng.lng]
    }

    this.launchNavigator.navigate([3.448619, -76.478127], options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  private async getLocation() {
    const currentP = await this.geolocation.getCurrentPosition();
    return {
      lat: currentP.coords.latitude,
      lng: currentP.coords.longitude
    };
  }

}
