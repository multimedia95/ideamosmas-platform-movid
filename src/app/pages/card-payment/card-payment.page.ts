import { Component, OnInit, Input } from '@angular/core';
import { PaymentModel } from 'src/app/models/payment.model';
import { OperationModel } from 'src/app/models/operation.model';
import { UserModel } from 'src/app/models/user.model';
import { CardInfoModel } from 'src/app/models/card-info.model';
import { ModalController } from '@ionic/angular';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { Storage } from '@ionic/storage';
import { OpenPayService } from 'src/app/services/openPay.service';
import { MessagingService } from 'src/app/services/messaging.service';
import { LoadingService } from 'src/app/services/loading.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-card-payment',
  templateUrl: './card-payment.page.html',
  styleUrls: ['./card-payment.page.scss'],
})
export class CardPaymentPage implements OnInit {

  authorizationToken: string;
  @Input() payment: PaymentModel;
  @Input() operation: OperationModel;
  user: UserModel;
  cardInformation: CardInfoModel;
  disabledbutton: boolean = false;


  constructor(
    private modalController: ModalController,
    private toastComponent: ToastComponent,
    private storage: Storage,
    private openPayService: OpenPayService,
    private messageService: MessagingService,
    private loadingService: LoadingService,
    private utilsService: UtilsService
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });
  }

  async ngOnInit() {
    this.loadingService.loadingPresent();
    this.storage.get('userInformation').then(resp => {
      this.user = resp;
    }).then(() => {
      if (this.user.openPayCardId) {
        this.openPayService.creditCardInfo(this.authorizationToken, this.user.openPayCardId).subscribe(
          data => {
            this.cardInformation = data;
            this.loadingService.loadingDismiss();
          }, error => {

          })
      }
    })
  }

  dismiss() {
    this.modalController.dismiss({
      dismissed: true
    });
  }

  onCreditCardPay() {
    this.disabledbutton = true;
    if (this.user.openPayCardId) {
      const deviceSessionId = OpenPay.deviceData.setup()
      const payment = {
        amount: this.payment.roundedTotal,
        currency: 'COP',
        customerId: this.user.openPayCustomerId,
        description: 'Pago de parqueadero',
        deviceSessionId: deviceSessionId,
        openPayCardId: this.user.openPayCardId,
        vat: this.payment.tax
      }

      this.openPayService.creditCardPayment(this.authorizationToken, payment).subscribe(data => {

        const paymentCalculatedDate = this.utilsService.convertDateForIos(this.payment.calculated);
        const payment = {
          amount: this.payment.amount,
          calculated: paymentCalculatedDate,
          moneyReceived: this.payment.amount,
          operationId: this.payment.operationId,
          parkingId: this.operation.parkingId,
          parkingTimeInSeconds: this.payment.parkingTimeInSeconds,
          roundedTotal: this.payment.roundedTotal,
          serviceId: this.payment.serviceId,
          setting: this.payment.setting,
          tax: this.payment.tax,
          total: this.payment.total
        }

        this.openPayService.savePayment(this.authorizationToken, payment).subscribe(data => {
          this.toastComponent.presentToast(
            'Exito en el pago.',
            'primary'
          );
          this.messageService.operationChange.emit(null);
        }, error => {
          this.toastComponent.presentToast(
            'Fallo en al realizar el pago.',
            'danger'
          );
          this.disabledbutton = false;
          console.log(error);
        })

      }, error => {
        this.toastComponent.presentToast(
          'Fallo en al realizar el pago.',
          'danger'
        );
        this.disabledbutton = false;
        console.log(error);
      })
    } else {
      this.toastComponent.presentToast(
        'No hay una tarjeta asociada para realizar el pago.',
        'warning'
      );
      this.disabledbutton = false;
    }
  }

  getCardImg(brand) {
    switch (brand) {
      case 'visa':
        return '../../../assets/img/Former_Visa_logo.svg';
      case 'mastercard':
        return '../../../assets/img/Mastercard-logo.svg';
      case 'american_express':
        return '../../../assets/img/American_Express_logo.svg';
      default:
        return '';
    }
  }

}
