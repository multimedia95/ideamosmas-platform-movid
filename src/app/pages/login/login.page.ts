import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Router, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserLogin } from 'src/app/models/user-login.model';
import { Storage } from '@ionic/storage';
import { AccessService } from 'src/app/services/access.service';
import { UserModel } from 'src/app/models/user.model';
import { GoogleLoginModel } from 'src/app/models/google-login.model';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { SocialMediaLogin } from 'src/app/models/social-media-login.model';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  submitAttempt: boolean = false;
  user: UserModel;
  socialMediaUser: SocialMediaLogin;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private googlePlus: GooglePlus,
    private storage: Storage,
    private fb: Facebook,
    private toastController: ToastComponent,
    private loadingController: LoadingService,
    private userService: AccessService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group(
      {
        email: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'), Validators.required]],
        password: ['', Validators.required]
      }
    );
  }

  ngOnInit() { }

  loginGoogle() {
    this.googlePlus.login({})
      .then((res: GoogleLoginModel) => {
        this.socialMediaUser = {
          email: res.email,
          socialId: res.userId,
          socialOrigin: 'Google',
          socialToken: res.idToken,
          firstName: res.givenName,
          lastName: res.familyName
        }
        this.validateSocialLogin(this.socialMediaUser);
      })
      .catch(err => this.toastController.presentToast('Error al iniciar sesión con google.', 'danger'));
  }

  validateSocialLogin(socialMediaUser: SocialMediaLogin) {
    this.authService.validateSocialLogin(socialMediaUser).subscribe(data => {
      if (data.registered == 'yes') {
        this.storage.set('Expiration', data.expiration);
        this.storage.set('Authorization', data.authorizationToken);
        this.userService.getUserInformation(data.authorizationToken).subscribe(data => {
          this.storage.set('userInformation', data);
          this.router.navigateByUrl('');
        }, error => {
          console.log(error);
        })
      } else {
        let navigationExtras: NavigationExtras = {
          state: {
            userData: this.socialMediaUser
          }
        }
        this.router.navigate(['/register'], navigationExtras);
      }

    }, error => {
      console.log(JSON.stringify(error))
    })

  }

  loginFaceBook() {
    this.fb.getLoginStatus().then((res) => {
      if (res.status === 'connected') {
        this.router.navigateByUrl('');
      } else {
        this.fb.login(['public_profile', 'email'])
          .then((res: FacebookLoginResponse) => {
            this.socialMediaUser = {
              email: '',
              socialId: res.authResponse.userID,
              socialOrigin: 'Facebook',
              socialToken: res.authResponse.accessToken,
              firstName: '',
              lastName: ''
            }
            console.log(res);
            this.validateSocialLogin(this.socialMediaUser);
          })
          .catch(e => this.toastController.presentToast('Error al iniciar sesión con facebook.', 'danger'));
      }
    })
  }

  onLogin() {
    this.loadingController.loadingPresent();
    this.submitAttempt = true;
    if (this.loginForm.valid) {
      const userLogin: UserLogin = {
        email: this.loginForm.value.email,
        firebaseToken: null,
        imei: '',
        password: this.loginForm.value.password
      }
      this.authService.passwordLogin(userLogin).subscribe(data => {
        this.authService.saveAuthorizationToken(data);
        this.userService.getUserInformation(data.authorizationToken).subscribe(
          data => {
            this.loadingController.loadingDismiss();
            this.user = data;
            this.storage.set('userInformation', this.user);
            this.router.navigateByUrl('');
          }, error => {
            console.log(error);
          }
        )
      }, error => {
        console.log(error.error.message);
        this.loadingController.loadingDismiss();
        this.toastController.presentToast(error.error.message, 'danger');
      })
    }
  }

}
