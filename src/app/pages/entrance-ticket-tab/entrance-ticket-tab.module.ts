import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EntranceTicketTabPageRoutingModule } from './entrance-ticket-tab-routing.module';

import { EntranceTicketTabPage } from './entrance-ticket-tab.page';

import { QRCodeModule } from 'angularx-qrcode'
import { ParkingTicketComponent } from './parking-ticket/parking-ticket.component';
import { PipeModule } from 'src/app/pipes/pipe.module';
import { ParkingSummaryComponent } from './parking-summary/parking-summary.component';
import { ParkingBillComponent } from './parking-bill/parking-bill.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QRCodeModule,
    PipeModule,
    EntranceTicketTabPageRoutingModule
  ],
  declarations: [
    EntranceTicketTabPage,
    ParkingTicketComponent,
    ParkingSummaryComponent,
    ParkingBillComponent
  ]
})
export class EntranceTicketTabPageModule { }
