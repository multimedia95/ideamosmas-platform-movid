import { Component, OnInit } from '@angular/core';
import { OtpModel } from 'src/app/models/otp.model';
import { UserModel } from 'src/app/models/user.model';
import { AccessService } from 'src/app/services/access.service';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { Storage } from '@ionic/storage';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-parking-ticket',
  templateUrl: './parking-ticket.component.html',
  styleUrls: ['./parking-ticket.component.scss'],
})
export class ParkingTicketComponent implements OnInit {

  code: string;
  isAValidCode: boolean;
  authorizationToken: string;
  otp: OtpModel = new OtpModel();
  user: UserModel = new UserModel();
  constructor(
    private storage: Storage,
    private parkingService: AccessService,
    private toastComponent: ToastComponent,
    private utilsService: UtilsService
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
      this.getCodeInformation(resp);
      this.getUserInformation();
    });
  }

  ngOnInit() { }

  codeRequest(authorizationToken: string) {
    return this.parkingService.getInformationCode(authorizationToken);
  }

  getCodeInformation(authorizationToken: string) {
    if (!authorizationToken) {
      return;
    }
    this.codeRequest(authorizationToken).subscribe((data: OtpModel) => {
      this.setQrCode(data);
    });
  }

  getUserInformation() {

    this.storage.get('userInformation').then(data => {
      this.user = data;
    });

  }

  setQrCode(data) {
    this.otp = data;
    const dateExpiration = this.utilsService.convertDateForIos(data.expirationTime);
    this.code = JSON.stringify(data);
    this.isAValidCode = true;

    if (dateExpiration.getTime() > new Date().getTime()) {
      const timeOut =
        dateExpiration.getTime() - new Date().getTime();

      setTimeout(() => {
        this.isAValidCode = false;
      }, timeOut);
    } else {
      this.isAValidCode = false;
    }
  }

  updateParkingTicket(event) {
    if (!this.authorizationToken) {
      event.target.complete();
      return;
    }
    this.codeRequest(this.authorizationToken).subscribe((data: OtpModel) => {
      this.setQrCode(data);
      this.toastComponent.presentToast(
        'Actualizamos tu tiquete para ingresar.',
        'primary'
      );
      event.target.complete();
    });
  }

}
