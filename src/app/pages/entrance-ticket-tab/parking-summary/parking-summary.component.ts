import { Component, OnInit, Input, NgZone } from '@angular/core';
import { PaymentModel } from 'src/app/models/payment.model';
import { OperationModel } from 'src/app/models/operation.model';
import { UserModel } from 'src/app/models/user.model';
import { CardInfoModel } from 'src/app/models/card-info.model';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { OpenPayService } from 'src/app/services/openPay.service';
import { environment } from 'src/environments/environment';
import { ModalController } from '@ionic/angular';
import { PaymentService } from 'src/app/services/payment.service';
import { Storage } from '@ionic/storage';
import { CardPaymentPage } from '../../card-payment/card-payment.page';
import { QrCodePage } from '../../qr-code/qr-code.page';

@Component({
  selector: 'app-parking-summary',
  templateUrl: './parking-summary.component.html',
  styleUrls: ['./parking-summary.component.scss'],
})
export class ParkingSummaryComponent implements OnInit {
  authorizationToken: string;
  @Input() payment: PaymentModel;
  @Input() operation: OperationModel;
  @Input() paymentEnabled: boolean;
  user: UserModel;
  cardInformation: CardInfoModel;

  constructor(
    public modalController: ModalController,
    private paymentService: PaymentService,
    private _ngZone: NgZone,
    private toastComponent: ToastComponent,
    private storage: Storage,
    private openPayService: OpenPayService
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });
  }

  ngOnInit() {
    OpenPay.setId(environment.openPayMerchantId);
    OpenPay.setApiKey(environment.openPayApiKey);
    OpenPay.setSandboxMode(true);
    console.log('ionViewWillEnter');
    this.storage.get('userInformation').then(resp => {
      this.user = resp;
    }).then(() => {
      if (this.user.openPayCardId) {
        this.openPayService.creditCardInfo(this.authorizationToken, this.user.openPayCardId).subscribe(
          data => {
            this.cardInformation = data;
          }, error => {

          })
      }
    })

    if (this.paymentEnabled === false) {
      this.presentModal();
      console.log('this.payment: ' + this.payment);
    }
  }

  secondsTohhmmss(totalSeconds) {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds - hours * 3600) / 60);
    return hours + 'h. ' + minutes + 'min. ';
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: QrCodePage,
      componentProps: {
        code: this.operation.code,
        noPayment: !this.paymentEnabled
      }
    });
    return await modal.present();
  }

  async presentModalCardPayment() {
    const modal = await this.modalController.create({
      component: CardPaymentPage,
      componentProps: {
        payment: this.payment,
        operation: this.operation
      }
    });
    return await modal.present();
  }

  updateParkingSummary(event) {
    if (!this.authorizationToken) {
      return;
    }
    this.payment = null;

    this.paymentService
      .getPaymentInformation(this.authorizationToken, this.operation)
      .subscribe(
        (data: PaymentModel) => {
          this._ngZone.run(() => {
            this.payment = new PaymentModel();
            this.payment = data;
          });
        },
        err => {
          this.toastComponent.presentToast(err.error.message, 'warning');
        }
      );
    event.target.complete();
  }

}
