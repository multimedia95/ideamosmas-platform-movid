import { Component, OnInit, NgZone } from '@angular/core';
import { PaymentModel } from 'src/app/models/payment.model';
import { OperationModel } from 'src/app/models/operation.model';
import { PaymentInformationModel } from 'src/app/models/payment-information.model';
import { AccessService } from 'src/app/services/access.service';
import { MessagingService } from 'src/app/services/messaging.service';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { UtilsService } from 'src/app/services/utils.service';
import { PaymentService } from 'src/app/services/payment.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';

enum OperationState {
  ENTERED,
  PAID,
  LEFT
}

enum OperationType {
  ENTRANCE,
  PAYMENT,
  EXIT,
  LEFT
}
@Component({
  selector: 'app-entrance-ticket-tab',
  templateUrl: './entrance-ticket-tab.page.html',
  styleUrls: ['./entrance-ticket-tab.page.scss'],
})
export class EntranceTicketTabPage implements OnInit {

  state = 'parkingTicket';
  paymentEnabled = false;
  authorizationToken: string;
  payment: PaymentModel;
  operation: OperationModel;
  paymentInformation: PaymentInformationModel;

  constructor(
    private parkingService: AccessService,
    private paymentService: PaymentService,
    private messagingService: MessagingService,
    private storage: Storage,
    private ngZone: NgZone,
    private toastComponent: ToastComponent,
    private modalController: ModalController,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    this.messagingService.receiveMessage().subscribe(data => {
      if (data.type) {
        switch (data.type) {
          case OperationType[OperationType.ENTRANCE]:
            this.setParkingSummaryPage(JSON.parse(data.operation));
            break;
          case OperationType[OperationType.PAYMENT]:
            this.setParkingBillPage(JSON.parse(data.payment));
            break;
          case OperationType[OperationType.EXIT]:
            this.setParkingTicketPage();
            break;
          case OperationType[OperationType.LEFT]:
            this.setParkingTicketPage();
            break;
        }
        console.log(JSON.stringify(data))
      } else {
        console.log(JSON.stringify(data))
      };

    });

    this.messagingService.operationChange.subscribe(
      () => {
        this.setHomePage();
      }
    )
  }

  ionViewWillEnter() {
    this.setHomePage();
    console.log('ionViewDidEnter');
  }

  private async getPaymentInformation() {
    if (!this.authorizationToken) {
      return;
    }
    this.paymentService
      .getPaymentInformation(this.authorizationToken, this.operation)
      .subscribe(
        (data: PaymentModel) => {
          this.ngZone.run(() => {
            this.payment = new PaymentModel();
            this.payment = data;
          });
        },
        err => {
          this.ngZone.run(() => {
            this.payment = new PaymentModel();
          });
          if (this.paymentEnabled) {
            this.toastComponent.presentToast(err.error.message, 'warning');
          }
        }
      );
  }

  private async goToParkingSummary(data) {
    this.operation = new OperationModel();
    this.operation.parkingId = data.parkingId;
    this.operation.additionalCode = data.additionalCode;
    this.operation.code = data.code;
    await this.getPaymentInformation();
  }

  setHomePage() {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
      this.parkingService
        .getLastOperation(this.authorizationToken)
        .subscribe(operation => {
          switch (operation.state) {
            case OperationState[OperationState.ENTERED]:
              this.setParkingSummaryPage(operation);
              break;
            case OperationState[OperationState.PAID]:
              this.parkingService
                .getLastPayment(this.authorizationToken, operation.id)
                .subscribe((payment: PaymentModel) => {
                  const authorizedExitDate = this.utilsService.convertDateForIos(payment.authorizedExit);
                  if (
                    authorizedExitDate >
                    new Date()
                  ) {
                    this.setParkingBillPage(payment);
                  } else {
                    this.setParkingSummaryPage(operation);
                  }

                });
              break;
            case OperationState[OperationState.LEFT]:
              this.modalController.dismiss({
                dismissed: true
              }).then(() => {
                this.toastComponent.presentToast(
                  'Gracias por preferirnos. ¡Vuelve pronto!',
                  'primary'
                );
                this.state = 'parkingTicket';
              });
              break;
            default:
              this.state = 'parkingTicket';
              break;
          }
        });
    });
  }

  setParkingBillPage(payment) {
    this.paymentInformation = payment;
    this.ngZone.run(() => {
      this.state = 'parkingBill';
    });
    this.operation = null;
    this.modalController.dismiss({
      dismissed: true
    });
  }

  setParkingTicketPage() {
    this.ngZone.run(() => {
      this.state = 'parkingTicket';
    });
    this.modalController.dismiss({
      dismissed: true
    });
    this.operation = null;
    this.paymentInformation = null;
    this.toastComponent.presentToast(
      'Gracias por preferirnos. ¡Vuelve pronto!',
      'primary'
    );
  }

  setParkingSummaryPage(operation) {
    this.paymentEnabled = operation.defaultServiceId ? true : false;
    this.state = 'parkingSummary';
    this.goToParkingSummary(operation);
  }

}
