import { Component, OnInit, Input } from '@angular/core';
import { PaymentInformationModel } from 'src/app/models/payment-information.model';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { MessagingService } from 'src/app/services/messaging.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-parking-bill',
  templateUrl: './parking-bill.component.html',
  styleUrls: ['./parking-bill.component.scss'],
})
export class ParkingBillComponent implements OnInit {
  isAuthorizedExit: boolean;
  @Input() paymentInformation: PaymentInformationModel;
  constructor(
    private toastComponent: ToastComponent,
    private messageService: MessagingService,
    private utilsService: UtilsService
  ) { }

  ngOnInit() {
    this.validateDates();
    this.timeOut.bind(this);
  }
  ngOnChanges() {
    this.validateDates();
  }

  secondsTohhmmss(totalSeconds) {
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds - hours * 3600) / 60);
    return hours + 'h. ' + minutes + 'min. ';
  }

  validateDates() {
    this.isAuthorizedExit = true;
    const authorizedExitDate = this.utilsService.convertDateForIos(this.paymentInformation.authorizedExit);
    if (
      authorizedExitDate.getTime() >
      new Date().getTime()
    ) {
      const timeOut =
        authorizedExitDate.getTime() -
        new Date().getTime();

      setTimeout(() => {
        this.timeOut();
      }, timeOut);
    } else {
      this.timeOut();
    }
  }

  timeOut() {
    this.isAuthorizedExit = false;
    this.toastComponent.presentToast(
      'Tu tiempo para salir terminó. Porfavor paga el recargo para poder salir.',
      'warning'
    );
    this.messageService.operationChange.emit(null);
  }

}
