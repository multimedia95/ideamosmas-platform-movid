import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EntranceTicketTabPage } from './entrance-ticket-tab.page';

const routes: Routes = [
  {
    path: '',
    component: EntranceTicketTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EntranceTicketTabPageRoutingModule {}
