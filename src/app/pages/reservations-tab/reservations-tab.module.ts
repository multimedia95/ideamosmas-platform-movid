import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationsTabPageRoutingModule } from './reservations-tab-routing.module';

import { ReservationsTabPage } from './reservations-tab.page';
import { ReservationCardComponent } from './reservation-card/reservation-card.component';
import { PipeModule } from 'src/app/pipes/pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservationsTabPageRoutingModule,
    PipeModule
  ],
  declarations: [
    ReservationsTabPage,
    ReservationCardComponent
  ]
})
export class ReservationsTabPageModule { }
