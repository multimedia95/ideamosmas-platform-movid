import { Component, OnInit, Input } from '@angular/core';
import { SubscriptionModel } from 'src/app/models/subscription.model';
import { Parking } from 'src/app/models/parking.model';
import { ParkingService } from 'src/app/services/parking.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-reservation-card',
  templateUrl: './reservation-card.component.html',
  styleUrls: ['./reservation-card.component.scss'],
})
export class ReservationCardComponent implements OnInit {

  @Input() subscription: SubscriptionModel;
  parking: Parking;

  constructor(
    private parkingService: ParkingService,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('Authorization').then(token => {
      this.parkingService.getParkingDetails(token, this.subscription.parkingId).subscribe(data => {
        this.parking = data;
      }, error => {

      })
    })
  }

}
