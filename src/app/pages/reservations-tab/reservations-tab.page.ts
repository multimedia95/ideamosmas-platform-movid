import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Storage } from '@ionic/storage';
import { UserModel } from 'src/app/models/user.model';
import { PaymentService } from 'src/app/services/payment.service';
import { LoadingService } from 'src/app/services/loading.service';
import { LaunchNavigatorOptions, LaunchNavigator } from '@ionic-native/launch-navigator/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-reservations-tab',
  templateUrl: './reservations-tab.page.html',
  styleUrls: ['./reservations-tab.page.scss'],
})
export class ReservationsTabPage implements OnInit {

  subscriptions: [];
  user: UserModel;
  authorizationToken: string;

  constructor(
    private router: Router,
    private storage: Storage,
    private paymentService: PaymentService,
    private loadingController: LoadingService
  ) { }

  async ngOnInit() {

    this.loadingController.loadingPresent();
    await this.storage.get('Authorization').then(token => {
      this.authorizationToken = token;
    });
    this.storage.get('userInformation').then(user => {
      this.user = user;
      this.paymentService.getSubscriptions(this.authorizationToken, this.user.id).subscribe(data => {
        this.subscriptions = data;
        this.loadingController.loadingDismiss();
      }, error => {
        console.log(error);
        this.loadingController.loadingDismiss();
      })
    });

  }

  onSelectReservation(subscription) {
    let navigationExtras: NavigationExtras = {
      state: {
        subscription
      }
    }
    this.router.navigateByUrl('/reservation-details', navigationExtras);
  }

}
