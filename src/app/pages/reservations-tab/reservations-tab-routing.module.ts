import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReservationsTabPage } from './reservations-tab.page';

const routes: Routes = [
  {
    path: '',
    component: ReservationsTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationsTabPageRoutingModule {}
