import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReservationsTabPage } from './reservations-tab.page';

describe('ReservationsTabPage', () => {
  let component: ReservationsTabPage;
  let fixture: ComponentFixture<ReservationsTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservationsTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReservationsTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
