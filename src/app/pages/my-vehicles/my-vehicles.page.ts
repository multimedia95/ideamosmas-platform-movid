import { Component, OnInit } from '@angular/core';
import { AccessService } from 'src/app/services/access.service';
import { Storage } from '@ionic/storage';
import { UserModel } from 'src/app/models/user.model';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { AlertController } from '@ionic/angular';
import { VehicleModel } from 'src/app/models/vehicle.model';

@Component({
  selector: 'app-my-vehicles',
  templateUrl: './my-vehicles.page.html',
  styleUrls: ['./my-vehicles.page.scss'],
})
export class MyVehiclesPage implements OnInit {

  authorizationToken: string;
  user: UserModel;

  constructor(
    private storage: Storage,
    private userService: AccessService,
    private toastController: ToastComponent,
    private alertController: AlertController
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.storage.get('userInformation').then(resp => {
      this.user = resp;
    })
  }

  onDeleteVehicle(vehicle: VehicleModel) {
    this.userService.deleteVehicle(this.authorizationToken, vehicle).subscribe(data => {
      if (vehicle.licensePlate === this.user.vehicleId) {
        this.userService.setFavoriteVehicle(this.authorizationToken, { vehicleId: '' }).subscribe(data => {
          this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
            this.storage.set('userInformation', data);
            this.user = data;
          }, error => {
            console.log(error);
          })
        }, error => {
          console.log(error);
        })
      } else {
        this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
          this.storage.set('userInformation', data);
          this.user = data;
        }, error => {
          console.log(error);
        })
      }
    }, error => {
      console.log(error);
      this.toastController.presentToast('Error al eliminar vehículo', 'danger')
    })
  }

  onSetFavorite(vehicleDetails: VehicleModel) {
    const vehicle = {
      vehicleId: vehicleDetails.licensePlate !== this.user.vehicleId ? vehicleDetails.licensePlate : ''
    }

    this.userService.setFavoriteVehicle(this.authorizationToken, vehicle).subscribe(data => {
      this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
        this.storage.set('userInformation', data);
        this.user = data;
      }, error => {
        console.log(error);
      })
    })
  }

  async presentAlertConfirm(vehicle: VehicleModel) {
    const alert = await this.alertController.create({
      header: 'Confirmación',
      message: 'Eliminar el vehículo con placa <strong>'
        + vehicle.licensePlate + '</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.onDeleteVehicle(vehicle)
          }
        }
      ]
    });
    await alert.present();
  }

}
