import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyVehiclesPageRoutingModule } from './my-vehicles-routing.module';

import { MyVehiclesPage } from './my-vehicles.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyVehiclesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [
    MyVehiclesPage
  ]
})
export class MyVehiclesPageModule { }
