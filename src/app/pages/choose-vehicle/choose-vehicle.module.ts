import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseVehiclePageRoutingModule } from './choose-vehicle-routing.module';

import { ChooseVehiclePage } from './choose-vehicle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseVehiclePageRoutingModule
  ],
  declarations: [ChooseVehiclePage]
})
export class ChooseVehiclePageModule {}
