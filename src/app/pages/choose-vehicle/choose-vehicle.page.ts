import { Component, OnInit } from '@angular/core';
import { ParkingTabService } from 'src/app/services/parking-tab.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose-vehicle',
  templateUrl: './choose-vehicle.page.html',
  styleUrls: ['./choose-vehicle.page.scss'],
})
export class ChooseVehiclePage implements OnInit {

  vehicles = [
    {
      path: '../../../assets/img/car.png',
      value: 'CAR',
      name: 'Carro'
    },
    {
      path: '../../../assets/img/truck.png',
      value: 'TRUCK',
      name: 'Camion'
    },
    {
      path: '../../../assets/img/motorcycle.png',
      value: 'MOTORCYCLE',
      name: 'Moto'
    },
    {
      path: '../../../assets/img/bicycle.png',
      value: 'BICYCLE',
      name: 'Bicicleta'
    }
  ]

  constructor(
    private parkingService: ParkingTabService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSelectVehicle(vehicle) {
    this.parkingService.vehiceleSelected.emit(vehicle.value);
    this.router.navigateByUrl("");
  }

}
