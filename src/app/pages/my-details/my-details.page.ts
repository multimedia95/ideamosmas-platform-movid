import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IonicSelectableComponent } from 'ionic-selectable';
import * as countriesFile from 'src/assets/json/countries.json'
import { Router, NavigationExtras } from '@angular/router';
import { SearchService } from 'src/app/services/search.service';
import { GooglePlaceModel } from 'src/app/models/google-place.model';
import { UserModel } from 'src/app/models/user.model';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-my-details',
  templateUrl: './my-details.page.html',
  styleUrls: ['./my-details.page.scss'],
})
export class MyDetailsPage implements OnInit {

  myDetailsForm: FormGroup;
  submitAttempt: boolean = false;
  countries: any = (countriesFile as any).default;
  user: UserModel;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private searchService: SearchService,
    private storage: Storage
  ) {

    this.myDetailsForm = this.formBuilder.group(
      {
        firstName: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        lastName: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        email: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        prefix: ['', [Validators.pattern('[0-9]*'), Validators.required]],
        cellphone: ['', [Validators.pattern('[0-9]*'), Validators.required]],
        cityOfResidence: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]]
      }
    );
  }

  ngOnInit() {
    this.searchService.citySelected.subscribe((data: GooglePlaceModel) => {
      this.myDetailsForm.controls['cityOfResidence'].setValue(data.structured_formatting.main_text)
    })
    this.storage.get('userInformation').then((data: UserModel) => {
      this.myDetailsForm.patchValue(
        {
          firstName: data.name,
          lastName: data.lastName,
          email: data.email,
          cellphone: data.phone
        }
      )
    })
  }

  onSaveDetails() {
    this.submitAttempt = true;
  }

  portChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }

  onFocus() {
    let navigationExtras: NavigationExtras = {
      state: {
        navigationBack: 'account/my-details',
        searchBy: 'cities'
      }
    }
    this.router.navigate(['/search-location'], navigationExtras);
  }

}
