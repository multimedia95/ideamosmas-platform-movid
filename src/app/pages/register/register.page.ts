import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as countriesFile from 'src/assets/json/countries.json';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { SocialMediaLogin } from 'src/app/models/social-media-login.model';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { Storage } from '@ionic/storage';
import { AccessService } from 'src/app/services/access.service';
import { MessagingService } from 'src/app/services/messaging.service';
import { Device } from '@ionic-native/device/ngx';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  submitAttempt: boolean = false;
  myDetailsForm: FormGroup;
  countries: any = (countriesFile as any).default;
  authorizationToken: string;
  socialMediaLogin = false;
  firebaseToken: string;
  imei: string;
  userMedia: SocialMediaLogin;
  singInAs: string = 'user';
  companyTypes = [
    {
      name: 'Centro comerciales',
      value: 'Malls'
    },
    {
      name: 'Centro de negocios',
      value: 'Bussiness center'
    },
    {
      name: 'Aeropuertos',
      value: 'Airports'
    },
    {
      name: 'Eventos y sitios deportivos',
      value: 'Events and sport sites'
    },
    {
      name: 'Parqueaderos',
      value: 'Parking'
    },
    {
      name: 'Gobierno y zonas reguladas',
      value: 'Government and regulated parking zones'
    },
    {
      name: 'Universidades y colegios',
      value: 'Universities and schools'
    },
  ]

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: AccessService,
    private toastController: ToastComponent,
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private messagingService: MessagingService,
    private device: Device
  ) {
    this.setImei();
    this.myDetailsForm = this.formBuilder.group(
      {
        firstName: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        lastName: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        email: ['', [Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'), Validators.required]],
        prefix: [{
          code: "CO",
          dial_code: "+57",
          name: "Colombia"
        },
        [Validators.required]],
        cellphone: ['', [Validators.pattern('[0-9]*'), Validators.required]],
        otp: [''],
        password: [''],
        passwordConfirmation: [''],
        typeCompany: [''],
        companyName: [''],
        typeUser: ['user'],
        allowCorrespondence: [false]
      }
    );

    this.messagingService.getToken().then(firebaseToken => {
      this.firebaseToken = firebaseToken;
    }, error => {
      console.log(error)
    });
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.userMedia = this.router.getCurrentNavigation().extras.state.userData;
        this.socialMediaLogin = true;
        this.myDetailsForm.patchValue({
          email: this.userMedia.email,
          firstName: this.userMedia.firstName,
          lastName: this.userMedia.lastName,
          socialId: this.userMedia.socialId,
          socialOrigin: this.userMedia.socialOrigin,
          socialToken: this.userMedia.socialToken
        })
      }
    })
  }

  setImei() {
    if (this.device.uuid) {
      this.imei = this.device.uuid;
    } else {
      this.imei = 'NOT FOUND';
    }
  }

  onRegister() {
    console.log(this.myDetailsForm.value.prefix);
    this.submitAttempt = true;
    if (this.myDetailsForm.valid &&
      ((this.myDetailsForm.value.password === this.myDetailsForm.value.passwordConfirmation
        && !!this.myDetailsForm.value.password && !!this.myDetailsForm.value.passwordConfirmation)
        || this.socialMediaLogin)) {
      const user = {
        email: this.myDetailsForm.value.email,
        firebaseToken: this.firebaseToken,
        imei: this.imei,
        lastName: this.myDetailsForm.value.lastName,
        name: this.myDetailsForm.value.firstName,
        otp: this.myDetailsForm.value.otp,
        phone: this.myDetailsForm.value.prefix.dial_code + this.myDetailsForm.value.cellphone,
        role: 'user',
        socialId: this.userMedia ? this.userMedia.socialId : '',
        socialOrigin: this.userMedia ? this.userMedia.socialOrigin : '',
        typeCompany: this.myDetailsForm.value.typeCompany,
        companyName: this.myDetailsForm.value.companyName,
        typeUser: this.myDetailsForm.value.typeUser,
        allowCorrespondence: this.myDetailsForm.value.allowCorrespondence,
        password: this.myDetailsForm.value.password
      }
      if (this.socialMediaLogin) {
        this.authService.register(user).subscribe(data => {
          this.storage.set('Authorization', data.authorizationToken);
          this.storage.set('Expiration', data.expiration);
          this.authorizationToken = data.authorizationToken;
          this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
            this.storage.set('userInformation', data);
            this.router.navigateByUrl('');
          }, error => {
            console.log(error);
          })
        }, error => {
          this.toastController.presentToast(error.error.message, 'danger');
        })
      } else {
        const otpRequest = {
          email: this.myDetailsForm.value.email,
          name: this.myDetailsForm.value.firstName,
          phone: this.myDetailsForm.value.cellphone
        }
        this.authService.otpRequest(otpRequest).subscribe(data => {
          let navigationExtras: NavigationExtras = {
            state: {
              user
            }
          }
          console.log(data);
          this.router.navigate(['/otp-confirmation'], navigationExtras);
        }, error => {
          this.toastController.presentToast(error.error.message, 'danger')
        })
      }

    }
  }

}
