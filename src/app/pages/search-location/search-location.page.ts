import { Component, OnInit, NgZone } from '@angular/core';
import { GooglePlaceModel } from 'src/app/models/google-place.model';
import { SearchService } from 'src/app/services/search.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-location',
  templateUrl: './search-location.page.html',
  styleUrls: ['./search-location.page.scss'],
})
export class SearchLocationPage implements OnInit {

  GoogleAutocomplete: google.maps.places.AutocompleteService;
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: string;
  config: any;
  myLatLng: google.maps.LatLng;

  constructor(
    private zone: NgZone,
    private searchService: SearchService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.config = this.router.getCurrentNavigation().extras.state;
        this.myLatLng = new google.maps.LatLng(
          this.router.getCurrentNavigation().extras.state.myLatLng.lat,
          this.router.getCurrentNavigation().extras.state.myLatLng.lng
        )
      }
    })
  }

  async ngOnInit() {
  }

  updateSearchResults() {
    if (this.autocomplete.input == '') {
      this.autocompleteItems = [];
      return;
    }
    let options = {
      input: '',
      types: [],
      location: this.myLatLng,
      radius: 10000
    };

    if (this.config.searchBy) {
      options = {
        input: this.autocomplete.input,
        types: ['(' + this.config.searchBy + ')'],
        location: this.myLatLng,
        radius: 10000
      };
    } else {
      options = {
        input: this.autocomplete.input,
        types: [],
        location: this.myLatLng,
        radius: 10000
      };
    }

    this.GoogleAutocomplete.getPlacePredictions(options,
      (predictions, status) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  selectSearchResult(item: google.maps.places.AutocompletePrediction) {
    this.location = item;
    this.placeid = this.location.place_id;
    if (this.config.searchBy) {
      this.searchService.citySelected.emit(item);
    } else {
      this.searchService.locationSelected.emit(item);
    }

    this.router.navigateByUrl(this.config.navigationBack);
  }


}
