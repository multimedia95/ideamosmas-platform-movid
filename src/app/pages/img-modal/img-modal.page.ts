import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ImgObjModel } from 'src/app/models/imgObj.model';

@Component({
  selector: 'app-img-modal',
  templateUrl: './img-modal.page.html',
  styleUrls: ['./img-modal.page.scss'],
})
export class ImgModalPage implements OnInit {

  img: ImgObjModel;

  @ViewChild('slider', { read: ElementRef }) slider: ElementRef;

  sliderOpts = {
    zoom: {
      maxRatio: 5
    }
  }

  constructor(
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.img = this.navParams.get('img');
  }

  zoom(zoomIn: boolean) {
    let zoom = this.slider.nativeElement.swiper.zoom;
    if (zoomIn) {
      zoom.in();
    } else {
      zoom.out();
    }
  }

  close() {
    this.modalController.dismiss();
  }

}
