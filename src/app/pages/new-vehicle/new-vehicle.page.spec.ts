import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewVehiclePage } from './new-vehicle.page';

describe('NewVehiclePage', () => {
  let component: NewVehiclePage;
  let fixture: ComponentFixture<NewVehiclePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVehiclePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewVehiclePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
