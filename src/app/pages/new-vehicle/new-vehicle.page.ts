import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AccessService } from 'src/app/services/access.service';
import { Storage } from '@ionic/storage';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { Router } from '@angular/router';

declare interface vehicle {
  documentNumber: string;
  documentType: string;
  licensePlate: string;
  vehicleType: string;
}

@Component({
  selector: 'app-new-vehicle',
  templateUrl: './new-vehicle.page.html',
  styleUrls: ['./new-vehicle.page.scss'],
})
export class NewVehiclePage implements OnInit {

  vehicleForm: FormGroup;
  submitAttempt: boolean = false;
  authorizationToken: string;

  constructor(
    private formBuilder: FormBuilder,
    private userService: AccessService,
    private storage: Storage,
    private toastService: ToastComponent,
    private router: Router
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });
  }

  ngOnInit() {
    this.vehicleForm = this.formBuilder.group(
      {
        vehicleType: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        licensePlate: ['', [Validators.pattern('[a-zA-Z0-9 ]*'), Validators.required]],
        documentType: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        documentNumber: ['', [Validators.pattern('[0-9]*'), Validators.required]]
      }
    )
  }

  saveNewVehicle() {
    this.submitAttempt = true;
    if (this.vehicleForm.valid) {
      this.userService.saveVehicle(this.authorizationToken, this.vehicleForm.value).subscribe(data => {
        this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
          this.storage.set('userInformation', data);
          this.router.navigateByUrl('/account/my-vehicles');
        }, error => {
          console.log(error)
        })
      }, error => {
        this.toastService.presentToast('Error al guardar el vehículo.', 'danger');
      })
    }
  }

}
