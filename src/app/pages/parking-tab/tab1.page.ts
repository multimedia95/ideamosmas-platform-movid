import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';
import { google } from "google-maps";
import { ParkingTabService } from 'src/app/services/parking-tab.service';
import { Router, NavigationExtras } from '@angular/router';
import { SearchService } from 'src/app/services/search.service';
import { ParkingMarkerModel } from 'src/app/models/parking-marker.model';
import { ParkingService } from 'src/app/services/parking.service';
import { Storage } from '@ionic/storage';
import { VehicleTypeModel } from 'src/app/models/vehicle-type.model';
import { format } from 'date-fns';

enum ParkingType {
  PUBLIC,
  PRIVATE
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  GoogleAutocomplete: google.maps.places.AutocompleteService;
  googleGeocode: google.maps.Geocoder;
  mapRef: google.maps.Map<HTMLElement>;
  icon = {
    url: '../../assets/icon/info.svg', // url
    scaledSize: new google.maps.Size(50, 50), // scaled size<
  };
  autocomplete: { input: string; };
  autocompleteItems: any[];
  location: any;
  placeid: any;
  selectedVehicle: VehicleTypeModel = {
    id: "c31dfaf2-533b-11ea-ae01-",
    creator: "felipego",
    modifier: null,
    created: "2020-02-19T22:17:53.000+0000",
    modified: null,
    state: "ENABLED",
    name: "CAR",
    summary: "Carro"
  };
  vehicles = [
    {
      id: "5e9889a42d99dd12886a3c5e",
      creator: "1130630969",
      modifier: null,
      created: "2020-04-16T16:36:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "SCOOTER",
      summary: "Patineta Electrica"
    },
    {
      id: "c31df8ae-533b-11ea-ae01-",
      creator: "felipego",
      modifier: null,
      created: "2020-02-19T22:17:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "BICYCLE",
      summary: "Bicicleta"
    },
    {
      id: "c31dfaf2-533b-11ea-ae01-",
      creator: "felipego",
      modifier: null,
      created: "2020-02-19T22:17:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "CAR",
      summary: "Carro"
    },
    {
      id: "c31dfbe8-533b-11ea-ae01-",
      creator: "felipego",
      modifier: null,
      created: "2020-02-19T22:17:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "MOTORCYCLE",
      summary: "Motocicleta"
    },
    {
      id: "c31dfd20-533b-11ea-ae01-",
      creator: "felipego",
      modifier: null,
      created: "2020-02-19T22:17:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "PUBLIC",
      summary: "Vehiculo de servicio publico"
    },
    {
      id: "c31dfd87-533b-11ea-ae01-",
      creator: "felipego",
      modifier: null,
      created: "2020-02-19T22:17:53.000+0000",
      modified: null,
      state: "ENABLED",
      name: "TRUCK",
      summary: "Camiones"
    }
  ]
  parkingArr: ParkingMarkerModel[];
  selectedParking: ParkingMarkerModel;
  authorizationToken = '';
  markers: google.maps.Marker[] = [];
  myLatLng: {
    lat: number;
    lng: number;
  };
  newLatLng: {
    lat: number;
    lng: number;
  };
  constructor(
    private geolocation: Geolocation,
    private loadingCtrl: LoadingController,
    private parkingTabService: ParkingTabService,
    private parkingService: ParkingService,
    private searchService: SearchService,
    private storage: Storage,
    private router: Router
  ) {
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
    this.googleGeocode = new google.maps.Geocoder();
    this.autocomplete = { input: '' };
    this.autocompleteItems = [];
    this.storage.get('Authorization').then(token => {
      this.authorizationToken = token;
    })
    this.storage.get('vehicleId').then(data => {
      if (data) {
        this.selectedVehicle = this.vehicles.find(v => v.id === data);
      }
    })
  }

  ngOnInit() {
    this.loadMap();
    this.parkingTabService.vehiceleSelected.subscribe(data => {
      this.selectedVehicle = this.vehicles.find(v => v.name === data);
      this.storage.set('vehicleId', this.selectedVehicle.id);
      this.setVehicleImg();
      this.getParkingMarkes();
    });
    this.searchService.locationSelected.subscribe(location => {
      this.placeid = location.place_id;
      this.googleGeocode.geocode({ placeId: location.place_id }, (results, status) => {
        if (status === "OK") {
          if (results[0]) {
            this.mapRef.setZoom(15);
            this.mapRef.setCenter(results[0].geometry.location);
            this.myLatLng = {
              lat: results[0].geometry.location.lat(),
              lng: results[0].geometry.location.lng()
            }
            this.getParkingMarkes();
          } else {
            console.log("No results found");
          }
        } else {
          console.log("Geocoder failed due to: " + status);
        }
      });
    })
  }

  async loadMap() {
    const loading = await this.loadingCtrl.create();
    loading.present();
    this.myLatLng = await this.getLocation();
    console.log(this.myLatLng);
    const mapEle: HTMLElement = document.getElementById('map');
    // create map
    this.mapRef = new google.maps.Map(mapEle, {
      center: this.myLatLng,
      zoom: 15,
      disableDefaultUI: true,
      clickableIcons: false
    });
    google.maps.event
      .addListenerOnce(this.mapRef, 'idle', () => {
        loading.dismiss();
        this.getParkingMarkes();
      });
    this.mapRef.addListener('dragend', () => {
      this.newLatLng = {
        lat: this.mapRef.getCenter().lat(),
        lng: this.mapRef.getCenter().lng(),
      }
      console.log('Center changed: ' + JSON.stringify(this.newLatLng));
      const distance = this.distance(this.myLatLng.lat, this.myLatLng.lng, this.newLatLng.lat, this.newLatLng.lng);
      console.log('Distance between origin and new point: ' + distance);
      if (distance > 20) {
        this.myLatLng = this.newLatLng;
        this.getParkingMarkes();
      }
    });

  }

  private addMarker(lat: number, lng: number, parking: ParkingMarkerModel) {
    let labelText = parking.dayPrice ? parking.dayPrice.toString() : ' ';
    const marker = new google.maps.Marker({
      icon: this.icon,
      position: {
        lat,
        lng
      },
      anchorPoint: new google.maps.Point(0, 0),
      map: this.mapRef
    });
    marker.addListener('click', () => {
      this.selectedParking = parking;
    });
    marker.setLabel({
      text: labelText,
      color: 'white'
    });
    this.markers.push(marker);
  }

  private async getLocation() {
    const currentP = await this.geolocation.getCurrentPosition();
    return {
      lat: currentP.coords.latitude,
      lng: currentP.coords.longitude
    };
  }

  getParkingMarkes() {
    console.log('Getting parking markers');
    this.parkingService.getParkingMarkers(
      format(Date.now(), 'yyyy-MM-dd'),
      format(Date.now(), 'HH:mm:ss'),
      this.myLatLng.lat.toString(),
      this.myLatLng.lng.toString(),
      this.selectedVehicle.id
    ).subscribe((data: ParkingMarkerModel[]) => {
      this.deleteMarkers();
      data.forEach(marker => {
        this.addMarker(marker.latitude, marker.longitude, marker);
      })
      console.log(data);
      this.showMarkers();
    }, error => {
      console.log(error);
    })
  }

  setMapOnAll(map: google.maps.Map | null) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }

  showMarkers() {
    this.setMapOnAll(this.mapRef);
  }

  ToParkingDetails() {
    this.router.navigate(['/parking-details/', this.selectedParking.id]);
  }

  setVehicleImg() {
    switch (this.selectedVehicle.name) {
      case 'CAR':
        return '../../../assets/img/car.png';
      case 'MOTORCYCLE':
        return '../../../assets/img/motorcycle.png';
      case 'BICYCLE':
        return '../../../assets/img/bicycle.png';
      case 'PUBLIC':
        return '../../../assets/img/public.png';
      case 'TRUCK':
        return '../../../assets/img/truck.png';
      default:
        return '';
    }
  }

  onFocus() {
    let navigationExtras: NavigationExtras = {
      state: {
        navigationBack: '',
        searchBy: '',
        myLatLng: this.myLatLng
      }
    }
    this.router.navigate(['/search-location'], navigationExtras);
  }

  distance(lat1, lon1, lat2, lon2) {
    const p = 0.017453292519943295;    // Math.PI / 180
    const c = Math.cos;
    const a = 0.5 - c((lat2 - lat1) * p) / 2 +
      c(lat1 * p) * c(lat2 * p) *
      (1 - c((lon2 - lon1) * p)) / 2;

    return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }

}
