import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentValidator } from 'src/app/validators/payment';
import { environment } from 'src/environments/environment';
import { OpenPayService } from 'src/app/services/openPay.service';
import { Storage } from '@ionic/storage';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { AccessService } from 'src/app/services/access.service';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading.service';

declare const OpenPay: any;

@Component({
  selector: 'app-new-payment-method',
  templateUrl: './new-payment-method.page.html',
  styleUrls: ['./new-payment-method.page.scss'],
})
export class NewPaymentMethodPage implements OnInit {

  expirationValidation: string = '';
  authorizationToken: string;
  cardForm: FormGroup;
  submitAttempt: boolean = false;
  deviceSessionId: string;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private storage: Storage,
    private userService: AccessService,
    private openPayService: OpenPayService,
    private toastComponent: ToastComponent,
    private loadingController: LoadingService,
    private router: Router
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });
  }

  ngOnInit() {
    OpenPay.setId(environment.openPayMerchantId);
    OpenPay.setApiKey(environment.openPayApiKey);
    OpenPay.setSandboxMode(true);
    console.log(OpenPay.getSandboxMode());
    this.deviceSessionId = OpenPay.deviceData.setup();

    this.cardForm = this.formBuilder.group(
      {
        card_number: ['', PaymentValidator.isValidCardNumber],
        holder_name: ['', [Validators.pattern('[a-zA-Z ]*'), Validators.required]],
        expiration_year: ['', PaymentValidator.isValidYear],
        expiration_month: ['', PaymentValidator.isValidMonth],
        cvv2: ['', PaymentValidator.isValidCVC],
        device_session_id: [this.deviceSessionId]
      }
    )
  }

  validateExpirationDate() {
    this.expirationValidation =
      OpenPay.card.validateExpiry(this.cardForm.get('expiration_month').value, this.cardForm.get('expiration_year').value) ? '' : 'Fecha de expiración invalida';
  }

  saveCreditCard() {
    this.loading = true;
    this.validateExpirationDate();
    this.loadingController.loadingPresent();
    this.submitAttempt = true;
    if (this.cardForm.valid) {
      OpenPay.token.create(this.cardForm.value, this.successCallback.bind(this), this.errorCallback.bind(this));
    }
  }

  successCallback(res) {
    const card = res.data;
    const body = {
      openPayCardId: card.id,
      openPayDeviceSessionId: this.deviceSessionId
    }

    this.openPayService.saveCreditCard(this.authorizationToken, body).subscribe(
      data => {
        this.toastComponent.presentToast(
          'Se guardo la tarjeta exitosamente',
          'primary'
        );
        this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
          this.storage.set('userInformation', data);
          this.router.navigateByUrl('/account/payment-methods');
          this.loadingController.loadingDismiss();
        })
      }, error => {
        this.loadingController.loadingDismiss();
        this.toastComponent.presentToast(
          'Fallo en el guardado de la tarjeta.',
          'warning'
        );
      }
    )
  }

  errorCallback(res) {
    this.toastComponent.presentToast(
      'Fallo en el guardado de la tarjeta.',
      'warning'
    );
  }

}
