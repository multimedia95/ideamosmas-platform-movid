import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-qrcode-modal',
  templateUrl: './qrcode-modal.page.html',
  styleUrls: ['./qrcode-modal.page.scss'],
})
export class QrcodeModalPage implements OnInit {

  data: any;

  constructor(
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.data = this.navParams.get('data');
  }

  close() {
    this.modalController.dismiss();
  }

}
