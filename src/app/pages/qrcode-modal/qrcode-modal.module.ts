import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrcodeModalPageRoutingModule } from './qrcode-modal-routing.module';

import { QrcodeModalPage } from './qrcode-modal.page';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrcodeModalPageRoutingModule,
    QRCodeModule
  ],
  declarations: [QrcodeModalPage]
})
export class QrcodeModalPageModule { }
