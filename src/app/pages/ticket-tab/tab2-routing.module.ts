import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
    children: [
      {
        path: 'reservations-tab',
        loadChildren: () => import('../reservations-tab/reservations-tab.module').then(m => m.ReservationsTabPageModule)
      },
      {
        path: 'entrance-ticket-tab',
        loadChildren: () => import('../entrance-ticket-tab/entrance-ticket-tab.module').then(m => m.EntranceTicketTabPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/entrance-ticket-tab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule { }
