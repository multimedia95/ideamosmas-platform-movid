import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { Storage } from '@ionic/storage';
import { AccessService } from 'src/app/services/access.service';

@Component({
  selector: 'app-otp-confirmation',
  templateUrl: './otp-confirmation.page.html',
  styleUrls: ['./otp-confirmation.page.scss'],
})
export class OtpConfirmationPage implements OnInit {

  user: any;
  otpCode: string;
  submitAttempt = false;
  authorizationToken: string;

  constructor(
    private authService: AuthService,
    private userService: AccessService,
    private toastController: ToastComponent,
    private storage: Storage,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user
      }
    })
  }

  ngOnInit() {
  }

  confirmOTP() {
    this.submitAttempt = true;
    if (this.otpCode) {
      this.user.otp = this.otpCode;
      this.authService.register(this.user).subscribe(data => {
        this.authService.saveAuthorizationToken(data);
        this.authorizationToken = data.authorizationToken;
        this.authService.savePassword(data.authorizationToken, { password: this.user.password }).subscribe(data => {
          this.userService.getUserInformation(this.authorizationToken).subscribe(data => {
            this.storage.set('userInformation', data);
            this.router.navigateByUrl('');
          }, error => {
            console.log(error);
          })
        }, error => {
          console.log(error);
        })
      }, error => {
        this.toastController.presentToast(error.error.message, 'danger');
      })
    }
  }

}
