import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { UserModel } from 'src/app/models/user.model';
import { OpenPayService } from 'src/app/services/openPay.service';
import { CardInfoModel } from 'src/app/models/card-info.model';

@Component({
  selector: 'app-payment-methods',
  templateUrl: './payment-methods.page.html',
  styleUrls: ['./payment-methods.page.scss'],
})
export class PaymentMethodsPage implements OnInit {

  authorizationToken: string;
  user: UserModel;
  cardInfo: CardInfoModel;

  constructor(
    private storage: Storage,
    private openPayService: OpenPayService
  ) {
    this.storage.get('Authorization').then(resp => {
      this.authorizationToken = resp;
    });

  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.storage.get('userInformation').then(resp => {
      this.user = resp;
      console.log(resp);
    }).then(() => {
      if (!this.user.openPayCardId) {

      } else {
        this.openPayService.creditCardInfo(this.authorizationToken, this.user.openPayCardId).subscribe(data => {
          this.cardInfo = data;
          console.log(data);
        }, error => {
          console.log(error);
        })
      }
    })
  }

}
