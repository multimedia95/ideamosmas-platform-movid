import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ParkingDetailsPageRoutingModule } from './parking-details-routing.module';

import { ParkingDetailsPage } from './parking-details.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicSelectableModule,
    ParkingDetailsPageRoutingModule
  ],
  declarations: [ParkingDetailsPage]
})
export class ParkingDetailsPageModule { }
