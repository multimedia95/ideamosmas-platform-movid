import { Component, OnInit } from '@angular/core';
import { Parking } from 'src/app/models/parking.model';
import { ModalController } from '@ionic/angular';
import { ImgModalPage } from '../img-modal/img-modal.page';
import { LocationService } from 'src/app/services/location.service';
import { ImgObjModel } from 'src/app/models/imgObj.model';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ParkingService } from 'src/app/services/parking.service';
import { PaymentService } from 'src/app/services/payment.service';
import { environment } from 'src/environments/environment';
import { Storage } from '@ionic/storage';
import { SubscriptionPriceModel } from 'src/app/models/subscription-price.model';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastComponent } from 'src/app/components/toast/toast.component';
import { TagModel } from 'src/app/models/Tag.model';
import { ScheduleGroupModel } from 'src/app/models/schedule-group.model';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-parking-details',
  templateUrl: './parking-details.page.html',
  styleUrls: ['./parking-details.page.scss'],
})
export class ParkingDetailsPage implements OnInit {



  parking: Parking;

  authorizationToken = '';

  parkingID = '';

  subscriptionPrices: SubscriptionPriceModel[];

  tags: TagModel[];

  parkingTags: TagModel[];

  scheduleGroups: ScheduleGroupModel[];

  selectedSubscription: SubscriptionPriceModel;

  quantity: number = 1;

  selectedVehicleId: string;

  reservation = {
    startDate: new Date().toISOString(),
    endDate: new Date().toISOString()
  }

  slideOpts = {
    slidesPerView: 1.1,
    centeredSlides: true,
    spaceBetween: 5
  }

  imageGallery: ImgObjModel[];

  parkingDocument: {
    description: string,
    nota: string
  };

  vehicleId: string;

  reservationPrice: number = 5000;

  constructor(
    private modalController: ModalController,
    private locationService: LocationService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private parkingService: ParkingService,
    private paymentService: PaymentService,
    private loadingService: LoadingService,
    private toastService: ToastComponent,
    private utilsService: UtilsService
  ) { }

  async ngOnInit() {
    this.loadingService.loadingPresent();
    this.parkingID = this.activedRoute.snapshot.paramMap.get('id');
    await this.storage.get('vehicleId').then(data => {
      this.vehicleId = data;
    }).then(() => {
      this.storage.get('Authorization').then(token => {
        this.authorizationToken = token;
        this.parkingService.getParkingDetails(this.authorizationToken, this.parkingID).subscribe(data => {
          this.parking = data;
          this.loadingService.loadingDismiss();
          this.paymentService.getSubscriptionPricesByCompany(this.authorizationToken, this.parking.companyId).subscribe((data: SubscriptionPriceModel[]) => {
            this.subscriptionPrices = data.filter(s => s.vehicleTypeId === this.vehicleId);
            console.log(data);
          }, error => {
            console.log(error);
          });
        }, error => {
          console.log(error);
          this.loadingService.loadingDismiss();
          this.toastService.presentToast('Error al cargar la informacion del parqueadero.', 'danger');
        })
        this.parkingService.getTags(this.authorizationToken).subscribe(data => {
          this.tags = data;
          this.parkingService.getParkingTags(this.authorizationToken, this.parkingID).subscribe((data: any[]) => {
            this.parkingTags = [];
            data.forEach(tag => {
              this.parkingTags.push(this.tags.find(el => el.id === tag.tagId));
            });
            console.log(this.parkingTags);
          }, error => {
            console.log(error);
          })
        }, error => {
          console.log(error);
        })
        this.paymentService.getScheduleGroup(this.authorizationToken, this.parkingID).subscribe(data => {
          this.scheduleGroups = data;
          console.log(data)
        }, error => {
          console.log(error);
        })

        this.locationService.findDocumentById(environment.locationTempToken, this.parkingID).subscribe(data => {
          this.parkingDocument = data;
          console.log(JSON.stringify(data));
        })
      })
    })
  }

  ionViewWillEnter() {
    const token = environment.locationTempToken;
    const parkingId = 'as4d5asd1a2s';
    this.locationService.findAllImagesObjectById(token, parkingId).subscribe(data => {
      this.imageGallery = data;
    })
  }

  openPreview(img) {
    this.modalController.create({
      component: ImgModalPage,
      componentProps: {
        img: img
      }
    }).then(modal => modal.present());
  }

  selectIcon(text) {
    switch (text) {
      case 'Vigilancia':
        return 'videocam';
      case 'Techado':
        return 'house_siding';
      default:
        return ''
    }
  }

  scheduleDays(from, to) {
    return this.utilsService.scheduleDays(from, to);
  }

  subscriptionTime(time) {
    switch (time) {
      case 'MONTH':
        return 'Mensualidad';
      case 'DAY':
        return 'Dia'
      case 'YEAR':
        return 'Anual'
      default:
        return time
    }
  }

  onReservation() {
    let navigationExtras: NavigationExtras = {
      state: {
        subscriptionPriceId: this.selectedSubscription.id,
        quantity: this.quantity,
        parking: this.parking,
        reservation: this.reservation,
      }
    }
    this.router.navigate([`/parking-details/${this.parking.id}/summary`], navigationExtras);
  }

}
