import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Parking } from 'src/app/models/parking.model';
import { PaymentService } from 'src/app/services/payment.service';
import { UserModel } from 'src/app/models/user.model';
import { OpenPayService } from 'src/app/services/openPay.service';
import { CardInfoModel } from 'src/app/models/card-info.model';
import { VehicleModel } from 'src/app/models/vehicle.model';
import { OpenpayPayment } from 'src/app/models/openpay-payment.model';
import { environment } from 'src/environments/environment';
import { SubscriptionModel } from 'src/app/models/subscription.model';
import { LoadingService } from 'src/app/services/loading.service';
import { ToastComponent } from 'src/app/components/toast/toast.component';

export class Calculate {
  companyId: string;
  parkingId: string;
  quantity: number;
  start: string;
  subscriptionPriceId: string;
}

export class SubscriptionCalculation {
  amount: number;
  tax: number;
  total: number;
  subscriptionPriceId: string;
  quantity: number;
  start: string;
  end: string
}

@Component({
  selector: 'app-summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit {

  parking: Parking;
  subscriptionPriceId: string;
  quantity: number;
  authorizationToken: string;
  subscriptionToCalculate: Calculate;
  reservationPrice = 5000;
  reservation = {
    startDate: new Date().toISOString(),
    endDate: new Date().toISOString()
  }
  subscriptionValue: SubscriptionCalculation;
  user: UserModel;
  cardInfo: CardInfoModel;
  selectedVehicle: VehicleModel;
  deviceSessionId: string;
  paymentMethodId: string;

  constructor(
    private storage: Storage,
    private router: Router,
    private route: ActivatedRoute,
    private paymentService: PaymentService,
    private openPayService: OpenPayService,
    private loadingService: LoadingService,
    private toastService: ToastComponent
  ) {
  }

  async ngOnInit() {
    this.loadingService.loadingPresent();
    OpenPay.setId(environment.openPayMerchantId);
    OpenPay.setApiKey(environment.openPayApiKey);
    OpenPay.setSandboxMode(true);
    console.log(OpenPay.getSandboxMode());
    this.deviceSessionId = OpenPay.deviceData.setup();

    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        const state = this.router.getCurrentNavigation().extras.state;
        this.parking = state.parking;
        this.reservation = state.reservation;
        this.quantity = state.quantity;
        this.subscriptionPriceId = state.subscriptionPriceId;
        this.subscriptionToCalculate = {
          companyId: this.parking.companyId,
          parkingId: this.parking.id,
          quantity: this.quantity,
          start: this.reservation.startDate,
          subscriptionPriceId: this.subscriptionPriceId
        }

      }
    })
    this.storage.get('Authorization').then(token => {
      this.authorizationToken = token;
      this.paymentService.calculateSubscription(this.authorizationToken, this.subscriptionToCalculate).subscribe(calculation => {
        console.log(JSON.stringify(calculation));
        this.subscriptionValue = calculation;
        this.loadingService.loadingDismiss();
      }, error => {
        console.log(error);
        this.loadingService.loadingDismiss();
      });
      this.paymentService.getPaymentMethods(this.authorizationToken).subscribe(data => {
        if (data.find(d => d.name === 'CREDIT CARD')) {
          this.paymentMethodId = data.find(d => d.name === 'CREDIT CARD').id;
        }
      }, error => {
        console.log(error);
      })
    })
    this.storage.get('userInformation').then(user => {
      this.user = user;
      if (this.user.vehicle.length && this.user.vehicleId) {
        if (this.user.vehicle.find(v => v.licensePlate === this.user.vehicleId)) {
          this.selectedVehicle = this.user.vehicle.find(v => v.licensePlate === this.user.vehicleId)
        }
      }
    }).then(() => {
      if (!this.user.openPayCardId) {

      } else {
        this.openPayService.creditCardInfo(this.authorizationToken, this.user.openPayCardId).subscribe(data => {
          this.cardInfo = data;
          console.log(data);
        }, error => {
          console.log(error);
        })
      }
    })
  }

  onBack() {
    return '/parking-details/' + this.parking?.id
  }

  getVehicleImg(type) {
    switch (type) {
      case 'CAR':
        return '../../../assets/img/car.png';
      case 'MOTORCYCLE':
        return '../../../assets/img/motorcycle.png';
      case 'BICYCLE':
        return '../../../assets/img/bicycle.png';
      case 'PUBLIC':
        return '../../../assets/img/public.png';
      case 'TRUCK':
        return '../../../assets/img/truck.png';
      default:
        return '';
    }
  }

  onPayReservation() {
    this.loadingService.loadingPresent();
    const payment: OpenpayPayment = {
      amount: (this.subscriptionValue.total + this.reservationPrice),
      currency: 'COP',
      customerId: this.user.openPayCustomerId,
      description: 'Pago suscripción Movid',
      deviceSessionId: this.deviceSessionId,
      openPayCardId: this.user.openPayCardId,
      vat: this.subscriptionValue.tax
    }
    this.openPayService.creditCardPayment(this.authorizationToken, payment).subscribe(data => {
      const subscription: SubscriptionModel = {
        additionalCode: this.user.id,
        amount: this.subscriptionValue.amount,
        companyId: this.parking.companyId,
        parkingId: this.parking.id,
        moneyReceived: this.subscriptionValue.total + this.reservationPrice,
        start: this.reservation.startDate,
        end: this.reservation.endDate,
        quantity: this.subscriptionValue.quantity,
        tax: this.subscriptionValue.tax,
        total: this.subscriptionValue.total + this.reservationPrice,
        paymentMethodId: this.paymentMethodId,
        subscriptionPriceId: this.subscriptionPriceId,
        vehiclePlate: this.selectedVehicle.licensePlate
      }
      this.paymentService.subscription(this.authorizationToken, subscription).subscribe(data => {
        this.toastService.presentToast('Se creo la suscripcion exitosamente.', 'primary');
        this.loadingService.loadingDismiss();
        console.log(data);
      }, error => {
        console.log(error);
        this.loadingService.loadingDismiss();
      })
    }, error => {
      console.log(error);
      this.toastService.presentToast('No se puedo realizar el pago.', 'danger');
      this.loadingService.loadingDismiss();
    })
  }

}
