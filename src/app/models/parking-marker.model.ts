export class ParkingMarkerModel {
    id: string;
    state: string;
    additionalInformation: string;
    address: string;
    configuration: string;
    email: string;
    exitTimeInSeconds: number;
    exitType: string;
    name: string;
    phone: number;
    policy: string;
    parkingType: string;
    latitude: number;
    longitude: number;
    taxResolutionId: string;
    companyId: string;
    categoryId: number;
    distance: number;
    photoLink: string;
    tags: string;
    dayPrice: number;
}