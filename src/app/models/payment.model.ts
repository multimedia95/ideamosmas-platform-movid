export class PaymentModel {
	calculated: Date;
	authorizedExit: Date;
	parkingTimeInSeconds: number;
	amount: number;
	serviceTax: number;
	tax: number;
	total: number;
	roundedTotal: number;
	setting: number;
	operationId: string;
	serviceId: string;
	expirationTime: number;
	vehicleType: string;
	serviceName: string;
}
