export class ImgObjModel {
    constructor(
        public idObject: string,
        public url: string
    ) { }
}