export class Parking {
    constructor(
        public additionalInformation: string,
        public address: string,
        public categoryId: string,
        public companyId: string,
        public configuration: string,
        public created: string,
        public creator: string,
        public email: string,
        public exitTimeInSeconds: number,
        public exitType: string,
        public id: string,
        public latitude: number,
        public longitude: number,
        public modified: string,
        public modifier: string,
        public name: string,
        public parkingType: string,
        public phone: string,
        public policy: string,
        public state: string,
        public taxResolutionId: string
    ) { }
}