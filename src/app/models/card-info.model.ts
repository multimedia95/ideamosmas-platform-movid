export class CardInfoModel {
    address: string;
    allowsCharges: boolean;
    allowsPayouts: boolean;
    bankCode: string;
    bankName: string;
    brand: string;
    cardNumber: string;
    creationDate: string;
    cvv2: string;
    deviceSessionId: string;
    expirationMonth: string;
    expirationYear: string;
    holderName: string;
    id: string;
    pointsCard: boolean;
    pointsType: string;
    tokenId: string;
    type: string;
}

