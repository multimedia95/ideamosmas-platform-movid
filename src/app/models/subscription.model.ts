export class SubscriptionModel {
    additionalCode: string;
    amount: number;
    end: string;
    moneyReceived: number;
    companyId: string;
    parkingId: string;
    quantity: number;
    start: string;
    paymentMethodId: string;
    subscriptionPriceId: string;
    tax: number;
    total: number;
    vehiclePlate: string
}
