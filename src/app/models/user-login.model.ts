export class UserLogin {
    email: string;
    firebaseToken: string;
    imei: string;
    password: string;
}