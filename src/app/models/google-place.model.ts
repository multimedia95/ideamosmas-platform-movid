export class GooglePlaceModel {
    description: string;
    id: string;
    place_id: string;
    reference: string;
    structured_formatting: {
        main_text: string;
        secondary_text: string;
    };
}
