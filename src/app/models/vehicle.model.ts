export class VehicleModel {
    documentNumber: string;
    documentType: string;
    licensePlate: string;
    vehicleType: string;
}