export class GoogleLoginModel {
    constructor(
        public email: string,
        public userId: string,
        public displayName: string,
        public familyName: string,
        public givenName: string,
        public imageUrl: string,
        public idToken: string,
        public serverAuthCode: string,
        public accessToken
    ) { }
}