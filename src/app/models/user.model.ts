import { VehicleModel } from './vehicle.model';

export class UserModel {
	id: string;
	email: string;
	name: string;
	lastName: string;
	imei: string;
	phone: string;
	otp: string;
	role: string;
	password: string;
	created: Date;
	status: string;
	firebaseToken: string;
	openPayCustomerId: string;
	openPayCardId: string;
	vehicleId: string;
	vehicle: VehicleModel[];
}