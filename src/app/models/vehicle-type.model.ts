export class VehicleTypeModel {
    id: string;
    creator: string;
    modifier: string;
    created: string;
    modified: string;
    state: string;
    name: string;
    summary: string;
}