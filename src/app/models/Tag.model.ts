export class TagModel {
    constructor(
        public id: string,
        public creator: string,
        public created: string,
        public modifier: string,
        public modified: string,
        public state: string,
        public description: string,
        public name: string
    ) { }
}