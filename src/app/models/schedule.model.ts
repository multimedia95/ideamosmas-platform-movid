export class ScheduleModel {
    created: string;
    creator: string;
    endDate: string;
    hourRangeFrom: string;
    hourRangeTo: string;
    id: string;
    modified: string;
    modifier: string;
    scheduleGroupId: string;
    startDate: string;
    state: string;
    weekdayFrom: number;
    weekdayTo: number;
}