export class SocialMediaLogin {
    email: string;
    socialId: string;
    socialOrigin: string;
    socialToken: string;
    firstName: string;
    lastName: string;
}