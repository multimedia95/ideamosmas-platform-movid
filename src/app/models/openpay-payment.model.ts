export class OpenpayPayment {
    amount: number;
    currency: string;
    customerId: string;
    description: string;
    deviceSessionId: string;
    openPayCardId: string;
    vat: number;
}

