export class SubscriptionPriceModel {
    companyId: string;
    cost: number;
    created: string;
    creator: string;
    id: string;
    modified: string;
    modifier: string;
    name: string;
    state: string;
    tax: number;
    time: string;
    vehicleTypeId: string;
}