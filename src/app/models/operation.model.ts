export class OperationModel {
	parkingId: string;
	code: string;
	additionalCode: string;
	state: string;
	id: string;
}
