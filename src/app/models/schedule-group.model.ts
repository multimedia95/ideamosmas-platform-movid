import { ScheduleModel } from './schedule.model';

export class ScheduleGroupModel {
    name: string;
    parkingId: string;
    scheduleGroupId: string;
    scheduleType: string;
    schedules: ScheduleModel[];
    state: string;
    summary: string;
}